# -*- coding: utf-8 -*-

import sys
from datetime import datetime
import random
import json

import unittest

if sys.version_info.major == 3:
    import unittest.mock as mock
else:
    import mock

import socket
from BuddyNS import BuddyNSAPI
from BuddyNS.errors import AuthenticationFailed, PermissionDenied, DoesNotExist


class TestAPI(unittest.TestCase):
    def prep_api(self, key='', validate_auth=False, status=None, response=None):
        bapi = BuddyNSAPI(key=key, validate_auth=validate_auth)
        bapi.connect = mock.MagicMock()

        if status is not None:
            bapi.connect().getresponse().status = status
        if response is not None:
            bapi.connect().getresponse().read.return_value = response
        
        return bapi

    def mk_response(self, cmd, n_zones=1, **kwargs):
        def mk_zname(i=0):
            return "z%s.it" % i

        def mk_time(ts=None):
            if ts is None:
                ts = datetime.now()
            return ts.strftime('%Y-%m-%dT%H:%M:%SZ')

        if cmd == 'list':
            ret = [{
                    "name": mk_zname(i),
                    "name_idn": mk_zname(i),
                    "serial": 2020041502,
                    "master": kwargs.get('master', "1.2.3.4"),
                    "creation_ts": "2020-04-15T19:10:17.844657",
                    "status": "http://www.buddyns.com/api/v2/zone/%s/status/" % mk_zname(i),
                    "delegation": "http://www.buddyns.com/api/v2/zone/%s/delegation/" % mk_zname(i)
                } for i in range(n_zones)]
        elif cmd == 'add':
            ret = {
                "name": kwargs.get('name', mk_zname()),
                "master": kwargs.get('master', "1.2.3.4")
            }
        elif cmd == 'remove':
            ret = {"detail": "xyz"}
        elif cmd == 'get':
            ret = {
                "name": kwargs.get('name', mk_zname()),
                "name_idn": kwargs.get('name', mk_zname()).encode('idna').decode(),
                "master": kwargs.get('master', '8.4.3.1'),
                "serial": kwargs.get('serial', 2019070402),
            }
        elif cmd == 'get_status':
            ret = {
                "zone": kwargs.get('name', mk_zname()),
                "transfer_ok": kwargs.get('transfer_ok', True),
                "first_failure_ts": mk_time(kwargs.get('ts', None)),
                "last_failure_ts": mk_time(kwargs.get('ts', None)),
                "last_success_ts": mk_time(kwargs.get('ts', None))
            }
        elif cmd == 'get_delegation':
            ret = {
                "zone": kwargs.get('name', mk_zname()),
                "master": kwargs.get('master', '1.2.3.4'),
                "master_ok": kwargs.get('master_ok', True),
                "registry_ok": kwargs.get('registry_ok', False),
                "authority_ok": kwargs.get('authority_ok', True)
            }
        elif cmd == 'user':
            ret = {
                'email': kwargs.get('email', 'support@buddyns.com'),
                "profile":"http://www.buddyns.com/api/v2/user/profile/",
                "billing":"http://www.buddyns.com/api/v2/user/billing/"
            }
        else:
            raise ValueError("Unknown command '%s'" % cmd)

        return json.dumps(ret).encode()


    # validate_authentication tests
    def test_validate_authentication_fails_timeout(self):
        bapi = self.prep_api()
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="validate_authentication() did not raise socket.timeout upon timeout."):
            bapi.validate_authentication()
        self.assertFalse(bapi.authenticated)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()
        
    def test_validate_authentication_fails_network_error(self):
        bapi = self.prep_api()
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="validate_authentication() did not raise exception upon network error."):
            bapi.validate_authentication()
        self.assertFalse(bapi.authenticated)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()
    
    def test_validate_authentication_succeeds(self):
        bapi = self.prep_api(status=200)
        bapi.connect().get_response.return_value = """{}"""
        bapi.validate_authentication()
        self.assertTrue(bapi.authenticated)
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_validate_authentication_fails_bad_credentials(self):
        bapi = self.prep_api(status=401)
        with self.assertRaises(AuthenticationFailed, msg="validate_authentication() did not raise exception upon 401 code."):
            bapi.validate_authentication()
        self.assertFalse(bapi.authenticated)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()


    # list_domains tests
    def test_list_timeout(self): # add timeout tests to other methods
        bapi = self.prep_api()
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="list_domains() did not raise socket.timeout upon timeout."):
            bapi.list_domains()
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_list_network_error(self): # add network_error to other methods
        bapi = self.prep_api()
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="list_domains() did not raise exception upon network error."):
            bapi.list_domains()
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_list_empty(self):
        n = 0
        bapi = self.prep_api(status=200, response=self.mk_response('list', n_zones=n))
        ret = bapi.list_domains()
        self.assertEqual(n, len(ret), msg="list_domains() returned '%s' elements upon %s-element JSON list." % (len(ret), n))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_list_one(self):
        n = 0
        bapi = self.prep_api(status=200, response=self.mk_response('list', n_zones=n))
        ret = bapi.list_domains()
        self.assertEqual(n, len(ret), msg="list_domains() returned '%s' elements upon %s-element JSON list." % (len(ret), n))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_list_many(self):
        n = 10*1000
        bapi = self.prep_api(status=200, response=self.mk_response('list', n_zones=n))
        ret = bapi.list_domains()
        self.assertEqual(n, len(ret), msg="list_domains() returned '%s' elements upon %s-element JSON list." % (len(ret), n))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_list_various(self):
        bapi = self.prep_api(status=200)
        for n in range(10):
            n = 3**n
            bapi.connect().getresponse().read.return_value = self.mk_response('list', n_zones=n)
            ret = bapi.list_domains()
            self.assertEqual(n, len(ret), msg="list_domains() returned '%s' elements upon %s-element JSON list." % (len(ret), n))


    # add_domain tests
    def test_add_new_domain_timeout(self):
        zname = 'foo.com'
        master = '5.2.9.0'
        bapi = self.prep_api(status=201, response=self.mk_response('add', name=zname, master=master))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="add_domain() did not raise socket.timeout upon timeout."):
            ret = bapi.add_domain(zname, master)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_add_new_domain_network_error(self):
        zname = 'foo.com'
        master = '5.2.9.0'
        bapi = self.prep_api(status=201, response=self.mk_response('add', name=zname, master=master))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="add_domain() did not raise exception upon network error."):
            ret = bapi.add_domain(zname, master)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_add_new_domain_succeeds(self):
        zname = 'foo.com'
        master = '5.2.9.0'
        bapi = self.prep_api(status=201, response=self.mk_response('add', name=zname, master=master))
        ret = bapi.add_domain(zname, master)
        self.assertIn('name', ret, msg="add_domain() expected to return dictionary containing 'name', got '%s' instead." % ret)
        self.assertIn('master', ret, msg="add_domain() expected to return dictionary containing 'master', got '%s' instead." % ret)
        self.assertEqual(zname, ret['name'], msg="add_domain() returned unexpected zone name '%s' instead of %s." % (ret['name'], zname))
        self.assertEqual(master, ret['master'], msg="add_domain() returned unexpected master '%s' instead of %s." % (ret['master'], master))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_add_fails_existing_zone(self):
        bapi = self.prep_api(status=400, response=self.mk_response('add'))
        with self.assertRaises(PermissionDenied, msg="add_domain() was expected to raise PermissionDenied upon 400 response."):
            ret = bapi.add_domain('foo.com', '1.2.3.4')
        bapi.connect().getresponse().read.assert_called_once_with()


    # remove_domain tests
    def test_remove_timeout(self):
        bapi = self.prep_api(status=204, response=self.mk_response('remove'))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="remove_domain() did not raise socket.timeout upon timeout."):
            bapi.remove_domain('foo.com')
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_remove_network_error(self):
        bapi = self.prep_api(status=204, response=self.mk_response('remove'))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="remove_domain() did not raise exception upon network error."):
            bapi.remove_domain('foo.com')
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_remove_succeeds_existing_zone(self):
        bapi = self.prep_api(status=204, response=self.mk_response('remove'))
        bapi.remove_domain('foo.com')
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_remove_domain_fails_missing_zone(self):
        bapi = self.prep_api(status=404, response=self.mk_response('remove'))
        with self.assertRaises(DoesNotExist, msg="remove_domain() was expected to raise DoesNotExist upon 400 response."):
            ret = bapi.remove_domain('foo.com')
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_remove_domain_fails_unauthorized(self):
        bapi = self.prep_api(status=400, response=self.mk_response('remove'))
        with self.assertRaises(PermissionDenied, msg="remove_domain() was expected to raise PermissionDenied upon 404 response."):
            ret = bapi.remove_domain('foo.com')
        bapi.connect().getresponse().read.assert_called_once_with()


    # get_domain tests
    def test_get_domain_timeout(self):
        zname = 'foo.com'
        master = '1.2.3.4'
        serial = 98765
        bapi = self.prep_api(status=200, response=self.mk_response('get', name=zname, master=master, serial=serial))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="get_domain() did not raise socket.timeout upon timeout."):
            ret = bapi.get_domain(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_network_error(self):
        zname = 'foo.com'
        master = '1.2.3.4'
        serial = 98765
        bapi = self.prep_api(status=200, response=self.mk_response('get', name=zname, master=master, serial=serial))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="get_domain() did not raise exception upon network error."):
            ret = bapi.get_domain(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_succeeds_existing(self):
        zname = 'foo.com'
        master = '1.2.3.4'
        serial = 98765
        bapi = self.prep_api(status=200, response=self.mk_response('get', name=zname, master=master, serial=serial))
        ret = bapi.get_domain(zname)
        self.assertIn('name', ret, msg="get_domain()'s return value lacks member 'name': %s" % ret)
        self.assertIn('master', ret, msg="get_domain()'s return value lacks member 'master': %s" % ret)
        self.assertIn('serial', ret, msg="get_domain()'s return value lacks member 'serial': %s" % ret)
        self.assertEqual(zname, ret.get('name'), msg="get_domain() expected to return name=%s, gave %s instead." % (zname, ret['name']))
        self.assertEqual(zname, ret.get('name_idn'), msg="get_domain() expected to return name_idn=%s, gave %s instead." % (zname, ret['name_idn']))
        self.assertEqual(serial, ret.get('serial'), msg="get_domain() expected to return serial=%s, gave %s instead." % (serial, ret['serial']))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_succeeds_existing_idn(self):
        zname = 'bücher.de' if sys.version_info.major == 3 else u'bücher.de'
        zname_idn = zname.encode('idna').decode()
        master = '1.2.3.4'
        serial = 98765
        bapi = self.prep_api(status=200, response=self.mk_response('get', name=zname, master=master, serial=serial))
        ret = bapi.get_domain(zname)
        self.assertIn('name', ret, msg="get_domain()'s return value lacks member 'name'.")
        self.assertIn('master', ret, msg="get_domain()'s return value lacks member 'master'.")
        self.assertIn('serial', ret, msg="get_domain()'s return value lacks member 'serial'.")
        self.assertEqual(zname, ret.get('name'), msg="get_domain() expected to return name=%s, gave %s instead." % (zname, ret['name']))
        self.assertEqual(zname_idn, ret.get('name_idn'), msg="get_domain() expected to return name=%s, gave %s instead." % (zname_idn, ret['name_idn']))
        self.assertEqual(serial, ret.get('serial'), msg="get_domain() expected to return serial=%s, gave %s instead." % (serial, ret['serial']))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_fails_missing(self):
        bapi = self.prep_api(status=404, response=self.mk_response('get'))
        with self.assertRaises(DoesNotExist, msg="get_domain() was expected to raise DoesNotExist upon missing zone."):
            ret = bapi.get_domain('foo.com')
        bapi.connect().getresponse().read.assert_called_once_with()
    

    # get_domain_status tests
    def test_get_domain_status_timeout(self):
        zname = 'buddyns.com'
        ts = datetime.now()
        bapi = self.prep_api(status=200, response=self.mk_response('get_status', name=zname, ts=ts, transfer_ok=True))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="get_domain_status() did not raise socket.timeout upon timeout."):
            ret = bapi.get_domain_status(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_status_network_error(self):
        zname = 'buddyns.com'
        ts = datetime.now()
        bapi = self.prep_api(status=200, response=self.mk_response('get_status', name=zname, ts=ts, transfer_ok=True))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="get_domain_status() did not raise exception upon network error."):
            ret = bapi.get_domain_status(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_status_succeeds_existing(self):
        zname = 'buddyns.com'
        ts = datetime.now()
        bapi = self.prep_api(status=200, response=self.mk_response('get_status', name=zname, ts=ts, transfer_ok=True))
        ret = bapi.get_domain_status(zname)
        self.assertEqual(zname, ret.get('zone', None), msg="get_domain_status() expected to return zone=%s, gave %s instead." % (zname, ret.get('zone', None)))
        self.assertTrue(ret.get('transfer_ok', None))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_status_fails_missing(self):
        bapi = self.prep_api(status=404)
        with self.assertRaises(DoesNotExist, msg="get_domain_status() was expected to raise DoesNotExist upon missing zone."):
            ret = bapi.get_domain_status('buddyns.com')
        bapi.connect().getresponse().read.assert_called_once_with()
    

    # get_domain_delegation tests
    def test_get_domain_delegation_timeout(self):
        zname = 'buddyns.com'
        bapi = self.prep_api(status=200, response=self.mk_response('get_delegation', name=zname, authority_ok=True, master_ok=True, registry_ok=False))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="get_domain_delegation() did not raise socket.timeout upon timeout."):
            ret = bapi.get_domain_delegation(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_delegation_network_error(self):
        zname = 'buddyns.com'
        bapi = self.prep_api(status=200, response=self.mk_response('get_delegation', name=zname, authority_ok=True, master_ok=True, registry_ok=False))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="get_domain_delegation() did not raise exception upon network error."):
            ret = bapi.get_domain_delegation(zname)
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_delegation_succeeds_existing(self):
        zname = 'buddyns.com'
        bapi = self.prep_api(status=200, response=self.mk_response('get_delegation', name=zname, authority_ok=True, master_ok=True, registry_ok=False))
        ret = bapi.get_domain_delegation(zname)
        self.assertEqual(zname, ret.get('zone', None), msg="get_domain_delegation() expected to return zone=%s, gave %s instead." % (zname, ret.get('zone', None)))
        self.assertTrue(ret.get('master_ok', None))
        self.assertFalse(ret.get('registry_ok', None))
        self.assertTrue(ret.get('authority_ok', None))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_domain_delegation_fails_missing(self):
        bapi = self.prep_api(status=404)
        with self.assertRaises(DoesNotExist, msg="get_domain_delegation() was expected to raise DoesNotExist upon missing zone."):
            ret = bapi.get_domain_status('buddyns.com')
        bapi.connect().getresponse().read.assert_called_once_with()
    

    # sync_domain tests
    def test_sync_domain_existing(self):
        bapi = self.prep_api(status=204, response="{}")
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="sync_domain() did not raise socket.timeout upon timeout."):
            ret = bapi.sync_domain('buddyns.com')
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_sync_domain_network_error(self):
        bapi = self.prep_api(status=204, response="{}")
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="sync_domain() did not raise exception upon network error."):
            ret = bapi.sync_domain('buddyns.com')
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_sync_domain_succeeds_existing(self):
        bapi = self.prep_api(status=204, response="{}")
        ret = bapi.sync_domain('buddyns.com')
        self.assertEqual(0, len(ret), msg="sync_domain() expected to return empty set, gave %s instead." % ret)
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_sync_domain_fails_missing(self):
        bapi = self.prep_api(status=404)
        with self.assertRaises(DoesNotExist, msg="sync_domain() was expected to raise DoesNotExist upon missing zone."):
            ret = bapi.sync_domain('buddyns.com')
        bapi.connect().getresponse().read.assert_called_once_with()
    

    # get_user tests
    def test_get_user_timeout(self):
        email = "support@buddyns.com"
        bapi = self.prep_api(status=200, response=self.mk_response('user', email=email))
        bapi.connect.side_effect = socket.timeout
        with self.assertRaises(socket.timeout, msg="get_user() did not raise socket.timeout upon timeout."):
            ret = bapi.get_user()
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_user_network_error(self):
        email = "support@buddyns.com"
        bapi = self.prep_api(status=200, response=self.mk_response('user', email=email))
        bapi.connect.side_effect = OSError
        with self.assertRaises(Exception, msg="get_user() did not raise exception upon network error."):
            ret = bapi.get_user()
        if bapi.conn is not None:
            bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_user_succeeds(self):
        email = "support@buddyns.com"
        bapi = self.prep_api(status=200, response=self.mk_response('user', email=email))
        ret = bapi.get_user()
        self.assertIn('email', ret, msg="get_user()'s return value lacks member 'email': %s" % ret)
        self.assertEqual(email, ret.get('email', None))
        bapi.connect().getresponse().read.assert_called_once_with()

    def test_get_user_fails_authentication(self):
        bapi = self.prep_api(status=401)
        with self.assertRaises(AuthenticationFailed, msg="get_user() was expected to raise AuthenticationFailed upon missing zone."):
            ret = bapi.get_user()
        bapi.connect().getresponse().read.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()